
DROP TABLE IF EXISTS schedules;
-- Create the table in the specified schema
CREATE TABLE schedules (
    id SERIAL NOT NULL PRIMARY KEY, -- primary key column
    user_id VARCHAR(255) NOT NULL,
    partners JSONB,
    details JSONB,
    date VARCHAR(255) NOT NULL,
    start_at VARCHAR(255) NOT NULL,
    end_at VARCHAR(255) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);