package main

import (
	"encoding/json"
	"log"
	"math"

	"github.com/jinzhu/gorm"
	"github.com/streadway/amqp"
)

// ScheduleController for rabbitMQ Schedules
type ScheduleController struct {
	*gorm.DB
}

// NewScheduleController creates a Schedule Controller.
func NewScheduleController(db *gorm.DB) *ScheduleController {
	return &ScheduleController{
		DB: db,
	}
}

func (c *ScheduleController) createSchedule() {
	GetFromMQ("front", "schedule.create", "topic", func(d amqp.Delivery) {
		data := NewScheduleRequest()
		json.Unmarshal(d.Body, &data)
		var checkForSchedule int
		c.DB.Model(&Schedule{}).Where(&Schedule{
			UserID:  data.UserID,
			Date:    data.Date,
			StartAt: data.StartAt,
			EndAt:   data.EndAt,
		}).Count(&checkForSchedule)
		if checkForSchedule == 0 {
			partners, _ := json.Marshal(data.Partners)
			details, _ := json.Marshal(data.Details)
			schedule := Schedule{
				UserID:   data.UserID,
				Partners: partners,
				Details:  details,
				Date:     data.Date,
				StartAt:  data.StartAt,
				EndAt:    data.EndAt,
			}
			c.DB.Create(&schedule)
			newdata, _ := json.Marshal(schedule)
			SendToMQ("schedules", "schedule.created", "topic", newdata)
		} else {
			newdata, _ := json.Marshal(map[string]interface{}{"err": "already booked"})
			SendToMQ("schedules", "schedule.create.error", "topic", newdata)
		}
	})
}

func (c *ScheduleController) getSchedulesByParams() {
	GetFromMQ("front", "schedule.get.by.params", "topic", func(d amqp.Delivery) {
		data := NewSchedulePaginatedRequest()
		json.Unmarshal(d.Body, &data)
		log.Printf("%s", data)
		var meetings []Schedule
		var mettingsCount float64
		offset := (data.Paginate.Page - 1) * data.Paginate.PerPage
		c.DB.Model(&Schedule{}).Where(&Schedule{
			UserID: data.Params.UserID,
		}).Order("id desc").Count(&mettingsCount).Offset(offset).Limit(data.Paginate.PerPage).Scan(&meetings)
		perPage := float64(data.Paginate.PerPage)
		lastPage := mettingsCount / perPage
		if math.Trunc(mettingsCount/perPage) != lastPage {
			lastPage = math.Trunc(mettingsCount/perPage) + 1
		}
		newdata, _ := json.Marshal(map[string]interface{}{"meetings": meetings, "paginator": map[string]interface{}{"lastPage": lastPage}})
		SendToMQ("schedules", "schedule.got.by.params", "topic", newdata)
	})
}
