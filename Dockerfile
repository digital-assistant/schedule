FROM golang

RUN go get golang.org/x/tools/cmd/goimports
RUN go get database/sql
RUN go get github.com/joho/godotenv
RUN go get github.com/lib/pq
RUN go get github.com/streadway/amqp
RUN go get -u github.com/jinzhu/gorm

RUN curl -sSfL https://raw.githubusercontent.com/cosmtrek/air/master/install.sh | sh -s

RUN alias air='~/.air'


WORKDIR /go/src/schedule

COPY . .

CMD go build && ./schedule
