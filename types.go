package main

import (
	"encoding/json"
	"time"
)

// Schedule model
type Schedule struct {
	ID        uint            `gorm:"AUTO_INCREMENT;PRIMARY_KEY"`
	UserID    string          `gorm:"NOT NULL"`
	Partners  json.RawMessage `sql:"type:json"`
	Details   json.RawMessage `sql:"type:json"`
	DeletedAt *time.Time      `sql:"index"`
	CreatedAt *time.Time
	UpdatedAt *time.Time
	Date      string
	StartAt   string
	EndAt     string
}

// PaginateRequest Body
type PaginateRequest struct {
	PerPage int
	Page    int
}

// ScheduleRequest request body
type ScheduleRequest struct {
	UserID   string
	StartAt  string
	EndAt    string
	Date     string
	Partners json.RawMessage
	Details  json.RawMessage `json:"object,omitempty"`
}

// PaginatedRequest Body
type PaginatedRequest struct {
	Params   ScheduleRequest
	Paginate PaginateRequest
}

// NewScheduleRequest create new instance with default values
func NewScheduleRequest() *ScheduleRequest {
	return &ScheduleRequest{
		UserID:   "",
		StartAt:  "",
		EndAt:    "",
		Date:     "",
		Partners: nil,
		Details:  nil,
	}
}

// NewSchedulePaginatedRequest create new instance with default values
func NewSchedulePaginatedRequest() *PaginatedRequest {
	return &PaginatedRequest{
		Params: ScheduleRequest{
			UserID:   "",
			StartAt:  "",
			EndAt:    "",
			Date:     "",
			Partners: nil,
			Details:  nil,
		},
		Paginate: PaginateRequest{
			PerPage: 10,
			Page:    1,
		},
	}
}
