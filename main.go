package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/streadway/amqp"
)

var db gorm.DB

// Callback function type
type Callback func(amqp.Delivery)

var (
	// DBName Database name
	DBName = "schedule"
	// DBUser Database user name
	DBUser = "schedule"
	// DBPassword Database password
	DBPassword = "schedule"
	// DBPort Port for database connection
	DBPort = "5432"
	// DBHost Hostname for database or IP
	DBHost = "db"
	// DBSSLMode SSL mode for connection
	DBSSLMode = "disable"

	//SERVER PORT
	listenPort = "3000"
)

func main() {
	resetVariables()
	dbinfo := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s", DBHost, DBPort, DBUser, DBPassword, DBName, DBSSLMode,
	)
	db, err := gorm.Open("postgres", dbinfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	c := NewScheduleController(db)
	c.createSchedule()
	c.getSchedulesByParams()

	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}

func resetVariables() {
	if len(os.Getenv("DB_NAME")) > 0 {
		DBName = os.Getenv("DB_NAME")
	}

	if len(os.Getenv("DB_USER")) > 0 {
		DBUser = os.Getenv("DB_USER")
	}

	if len(os.Getenv("DB_PASSWORD")) > 0 {
		DBPassword = os.Getenv("DB_PASSWORD")
	}

	if len(os.Getenv("DB_PORT")) > 0 {
		DBPort = os.Getenv("DB_PORT")
	}

	if len(os.Getenv("DB_HOST")) > 0 {
		DBHost = os.Getenv("DB_HOST")
	}

	if len(os.Getenv("DB_SSL_MODE")) > 0 {
		DBSSLMode = os.Getenv("DB_SSL_MODE")
	}
	if len(os.Getenv("PORT")) > 0 {
		listenPort = os.Getenv("PORT")
	}
}
